Sample Express-Mongodb REST API

This is a sample REST API built using Express and MongoDB, featuring comprehensive testing and documentation. It utilizes the following technologies and packages:

Database: MongoDB with Mongoose for data modeling and interaction.

Validation: AJV for input data validation.

Documentation: Swagger with swagger-jsdoc and swagger-ui-express for clear API documentation.

Testing: Jest, Supertest, and MongoDB Memory Server for robust and efficient testing.

A docker compose file for database setup and a postman collection for ease of use are also added to project.
