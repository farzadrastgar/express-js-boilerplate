import express from "express";
import helmet from "helmet";
import errorHandler from "./middlewares/errorHandler.js";
import productRouter from "./routes/product.route.js";
import swagger from "./utils/swagger.js";

const app = express();

app.use(helmet());
app.use(express.json());
app.use("/api/products", productRouter);
app.get("/health", (req, res) => res.sendStatus(200));

app.use(errorHandler);
swagger(app);

app.all("*", (req, res) => {
  res.status(404).json({
    success: false,
    message: "URL not found!",
  });
});

export default app;
