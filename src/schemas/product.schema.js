/**
 * @openapi
 * components:
 *   schema:
 *     getAllProductResponse:
 *        type: object
 *        properties:
 *          success:
 *            type: boolean
 *            example: true
 *          data:
 *            type: array
 *            items:
 *              type: object
 *              properties:
 *                ratings:
 *                  type: object
 *                  properties:
 *                    averageRating:
 *                      type: number
 *                      example: 4.8
 *                    numRatings:
 *                      type: integer
 *                      example: 120
 *                _id:
 *                  type: string
 *                  example: "651fc8e6d1469f42d94b3d15"
 *                productId:
 *                  type: string
 *                  example: "1"
 *                productName:
 *                  type: string
 *                  example: "Smartphone"
 *                description:
 *                  type: string
 *                  example: "A high-end smartphone with a large screen and fast processor."
 *                price:
 *                  type: number
 *                  example: 699.99
 *                category:
 *                  type: string
 *                  example: "Electronics"
 *                stockQuantity:
 *                  type: integer
 *                  example: 50
 *                manufacturer:
 *                  type: string
 *                  example: "TechGadget Corp"
 *     getIdProductResponse:
 *        type: object
 *        properties:
 *          success:
 *            type: boolean
 *            example: true
 *          data:
 *            type: object
 *            properties:
 *              ratings:
 *                type: object
 *                properties:
 *                  averageRating:
 *                    type: number
 *                    example: 4.8
 *                  numRatings:
 *                    type: integer
 *                    example: 120
 *              _id:
 *                type: string
 *                example: "651fc8e6d1469f42d94b3d15"
 *              productId:
 *                type: string
 *                example: "1"
 *              productName:
 *                type: string
 *                example: "Smartphone"
 *              description:
 *                type: string
 *                example: "A high-end smartphone with a large screen and fast processor."
 *              price:
 *                type: number
 *                example: 699.99
 *              category:
 *                type: string
 *                example: "Electronics"
 *              stockQuantity:
 *                type: integer
 *                example: 50
 *              manufacturer:
 *                type: string
 *                example: "TechGadget Corp"
 *
 *     updateProductRequest:
 *       type: object
 *       properties:
 *         ratings:
 *           type: object
 *           properties:
 *             averageRating:
 *               type: number
 *               example: 4.8
 *             numRatings:
 *               type: integer
 *               example: 120
 *         productId:
 *           type: string
 *           example: "1"
 *         productName:
 *           type: string
 *           example: "Smartphone"
 *         description:
 *           type: string
 *           example: "A high-end smartphone with a large screen and fast processor."
 *         price:
 *           type: number
 *           example: 699.99
 *         category:
 *           type: string
 *           example: "Electronics"
 *         stockQuantity:
 *           type: integer
 *           example: 50
 *         manufacturer:
 *           type: string
 *           example: "TechGadget Corp"
 *
 */

export const createProductSchema = {
  body: {
    type: "object",
    properties: {
      productId: { type: "string" },
      productName: { type: "string" },
      description: { type: "string" },
      price: { type: "number" },
      category: { type: "string" },
      stockQuantity: { type: "integer" },
      manufacturer: { type: "string" },
      ratings: {
        type: "object",
        properties: {
          averageRating: { type: "number" },
          numRatings: { type: "integer" },
        },
        required: ["averageRating", "numRatings"],
      },
    },
    required: ["productId", "productName", "price"],
  },
};

export const getAllProductSchema = {
  query: {
    type: "object",
    additionalProperties: false,
  },
};

export const getIdProductSchema = {
  params: {
    type: "object",
    properties: {
      id: { type: "string", format: "uuid" },
    },
    required: ["id"],
    additionalProperties: false,
  },
};

export const updateProductSchema = {
  params: {
    type: "object",
    properties: {
      id: { type: "string", format: "uuid" },
    },
    required: ["id"],
    additionalProperties: false,
  },
  body: {
    type: "object",
    properties: {
      productId: { type: "string" },
      productName: { type: "string" },
      description: { type: "string" },
      price: { type: "number" },
      category: { type: "string" },
      stockQuantity: { type: "integer" },
      manufacturer: { type: "string" },
      ratings: {
        type: "object",
        properties: {
          averageRating: { type: "number" },
          numRatings: { type: "integer" },
        },
        required: ["averageRating", "numRatings"],
      },
    },
    additionalProperties: false,
  },
};

export const deleteProductSchema = {
  params: {
    type: "object",
    properties: {
      id: { type: "string", format: "uuid" },
    },
    required: ["id"],
    additionalProperties: false,
  },
};
