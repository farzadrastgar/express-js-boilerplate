import logger from "../utils/logger.js";

const errorHandler = (err, req, res, next) => {
  logger.error(err);
  const errStatus = err.statusCode || 500;
  const errMsg = err.message || "Something went wrong";
  res.status(errStatus).json({
    success: false,
    message: errMsg,
    stack: process.env.NODE_ENV === "dev" ? err.stack : {},
  });
};

export default errorHandler;
