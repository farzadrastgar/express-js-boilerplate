import AJV from "ajv";
var ajv = new AJV();
import { isValidObjectId } from "mongoose";

ajv.addFormat("uuid", (data) => {
  return isValidObjectId(data);
});

const validate = function (schema) {
  // compile schema once

  return function (req, res, next) {
    try {
      let validationError = [];
      if ("query" in schema) {
        let validate = ajv.compile(schema.query);
        if (!validate(req.query)) {
          validationError = validate.errors;
        }
      }

      if ("body" in schema) {
        let validate = ajv.compile(schema.body);
        if (!validate(req.body)) {
          validationError = validate.errors;
        }
      }

      if ("params" in schema) {
        let validate = ajv.compile(schema.params);
        if (!validate(req.params)) {
          validationError = validate.errors;
        }
      }

      if (validationError.length === 0) {
        return next();
      }

      res.status(400).json({ error: validationError });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: "Server Error" });
    }
  };
};

export default validate;
