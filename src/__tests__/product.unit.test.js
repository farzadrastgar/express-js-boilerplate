import {
  updateProductHandler,
  getAllProductHandler,
  getIdProductHandler,
  createProductHandler,
  deleteProductHandler,
} from "../controllers/product.controller";
import { expect, jest } from "@jest/globals";
import ProductModel from "../models/product.model.js";

const saveSpy = jest.spyOn(ProductModel.prototype, "save");
const findOneSpy = jest.spyOn(ProductModel, "findOne");
const findOneAndDeleteSpy = jest.spyOn(ProductModel, "findOneAndDelete");
const findSpy = jest.spyOn(ProductModel, "find");

const sendMock = jest.fn((x) => x);
const jsonMock = jest.fn((x) => x);
const statusMock = jest.fn((x) => {
  return {
    send: sendMock,
    json: jsonMock,
  };
});

const saveMock = jest.fn((x) => x);
const sampleProduct = {
  productId: "88",
  productName: "Smartphoghghhne",
  description: "A high-end smartphone with a large screen and fast processor.",
  price: 699.99,
  category: "Electronics",
  stockQuantity: 50,
  manufacturer: "TechGadget Corp",
  ratings: {
    averageRating: 4.8,
    numRatings: 120,
  },
};

const request = {
  params: {
    id: "fake_id",
  },
  body: sampleProduct,
};

const response = {
  status: statusMock,
};

describe("POST /api/products creates a new product", () => {
  it("should return 200 and returns array of products", async () => {
    saveSpy.mockReturnValueOnce(sampleProduct);

    await createProductHandler(request, response);
    // expect(saveMock).toHaveBeenCalled();

    expect(jsonMock).toHaveBeenCalledWith({
      success: true,
      data: sampleProduct,
    });
    expect(response.status).toBeCalledTimes(1);
    expect(response.status).toHaveBeenCalledWith(201);
  });
});

describe("GET /api/products GET ALL products", () => {
  it("should return 200 and returns array of products", async () => {
    findSpy.mockReturnValueOnce([sampleProduct]);
    await getAllProductHandler({}, response);
    expect(findSpy).toBeCalledWith();
    expect(jsonMock).toHaveBeenCalledWith({
      success: true,
      data: [sampleProduct],
    });
    expect(response.status).toBeCalledTimes(1);
    expect(response.status).toHaveBeenCalledWith(200);
  });

  it("should return 200 and returns an empty array", async () => {
    findSpy.mockReturnValueOnce(null);
    await getAllProductHandler({}, response);
    expect(findSpy).toBeCalledWith();
    expect(jsonMock).toHaveBeenCalledWith({
      success: true,
      data: [],
    });
    expect(response.status).toBeCalledTimes(1);
    expect(response.status).toHaveBeenCalledWith(200);
  });
});

describe("GET /api/products/:id GET single product", () => {
  it("should return 200 and returns product", async () => {
    findOneSpy.mockReturnValueOnce(sampleProduct);
    await getIdProductHandler(request, response);
    expect(findOneSpy).toBeCalledWith({ _id: "fake_id" });
    expect(jsonMock).toHaveBeenCalledWith({
      success: true,
      data: sampleProduct,
    });
    expect(response.status).toBeCalledTimes(1);
    expect(response.status).toHaveBeenCalledWith(200);
  });

  it("should return 404 and returns an empty array", async () => {
    findOneSpy.mockReturnValueOnce(null);
    await getIdProductHandler(request, response);
    expect(findOneSpy).toBeCalledWith({ _id: "fake_id" });
    expect(jsonMock).toHaveBeenCalledWith({
      success: false,
      message: "Product not found",
    });
    expect(response.status).toBeCalledTimes(1);
    expect(response.status).toHaveBeenCalledWith(404);
  });
});

describe("PUT /api/products Update a product", () => {
  it("should return 200 and update product", async () => {
    findOneSpy.mockReturnValueOnce({ save: saveMock, ...sampleProduct });
    await updateProductHandler(request, response);
    expect(findOneSpy).toBeCalledWith({ _id: "fake_id" });
    expect(saveMock).toHaveBeenCalled();
    expect(jsonMock).toHaveBeenCalled();
    expect(response.status).toBeCalledTimes(1);
    expect(response.status).toHaveBeenCalledWith(200);
  });

  it("should return 404", async () => {
    findOneSpy.mockReturnValueOnce(null);
    await updateProductHandler(request, response);
    expect(response.status).toBeCalledTimes(1);
    expect(response.status).toHaveBeenCalledWith(404);
  });
});

describe("DELETE /api/products/:id DELETE a single product", () => {
  it("should return 200 and delete the product", async () => {
    findOneAndDeleteSpy.mockReturnValueOnce(sampleProduct);
    await deleteProductHandler(request, response);
    expect(findOneAndDeleteSpy).toBeCalledWith({ _id: "fake_id" });
    expect(jsonMock).toHaveBeenCalledWith({
      success: true,
    });
    expect(response.status).toBeCalledTimes(1);
    expect(response.status).toHaveBeenCalledWith(204);
  });

  it("should return 404 and returns a message", async () => {
    findOneAndDeleteSpy.mockReturnValueOnce(null);
    await deleteProductHandler(request, response);
    expect(findOneAndDeleteSpy).toBeCalledWith({ _id: "fake_id" });
    expect(jsonMock).toHaveBeenCalledWith({
      success: false,
      message: "Product not found",
    });
    expect(response.status).toBeCalledTimes(1);
    expect(response.status).toHaveBeenCalledWith(404);
  });
});
