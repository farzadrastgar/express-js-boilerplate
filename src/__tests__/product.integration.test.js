import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import request from "supertest";
import app from "../app";

const sampleProduct = {
  productId: "88",
  productName: "Smartphoghghhne",
  description: "A high-end smartphone with a large screen and fast processor.",
  price: 699.99,
  category: "Electronics",
  stockQuantity: 50,
  manufacturer: "TechGadget Corp",
  ratings: {
    averageRating: 4.8,
    numRatings: 120,
  },
};

let mongod;

beforeAll(async () => {
  mongod = await MongoMemoryServer.create();
  const uri = mongod.getUri();
  await mongoose.connect(uri);
});

afterAll(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongod.stop();
});

afterEach(async () => {
  const collections = mongoose.connection.collections;
  for (const key in collections) {
    const collection = collections[key];
    await collection.deleteMany();
  }
});

describe("POST /api/products Create a new product", () => {
  it("should create a product", async () => {
    const result = await request(app).post("/api/products").send(sampleProduct);
    expect(result.statusCode).toBe(201);
    expect(result.body.success).toBeTruthy();
  });

  it("should fail and give a validation error", async () => {
    const result = await request(app).post("/api/products").send({});
    expect(result.statusCode).toBe(400);
    expect(result.body.error.length).toBeGreaterThan(0);
  });
});

describe("GET /api/products", () => {
  it("should return an empty array", async () => {
    const result = await request(app).get("/api/products");
    expect(result.statusCode).toBe(200);
    expect(result.body.data.length).toBe(0);
  });

  it("should return all products", async () => {
    await request(app).post("/api/products").send(sampleProduct);
    const result = await request(app).get("/api/products");
    expect(result.statusCode).toBe(200);
    expect(result.body.data.length).toBeGreaterThan(0);
  });
});

describe("GET /api/products/:id", () => {
  it("should return a 400 invalid id", async () => {
    const result = await request(app).get("/api/products/invalid_id");
    expect(result.statusCode).toBe(400);
    expect(result.body.error.length).toBeGreaterThan(0);
  });

  it("should return a 404 product not found", async () => {
    const result = await request(app).get(
      "/api/products/65250ad5c3e05d0ddb5bbfa0"
    );
    expect(result.statusCode).toBe(404);
  });

  it("should return single product", async () => {
    let newProduct = await request(app)
      .post("/api/products")
      .send(sampleProduct);
    let _id = newProduct.body.data._id;
    const result = await request(app).get("/api/products/" + _id);
    expect(result.statusCode).toBe(200);
    expect(result.body.data).toHaveProperty("_id", _id);
    expect(result.body.data).toHaveProperty("productId", "88");
  });
});

describe("PUT /api/products/:id", () => {
  it("should return a 400 invalid id", async () => {
    const result = await request(app).put("/api/products/invalid_id");
    expect(result.statusCode).toBe(400);
    expect(result.body.error.length).toBeGreaterThan(0);
  });

  it("should return a 404 product not found", async () => {
    const result = await request(app)
      .put("/api/products/65250ad5c3e05d0ddb5bbfa0")
      .send(sampleProduct);
    expect(result.statusCode).toBe(404);
  });

  it("should update single product", async () => {
    let newProduct = await request(app)
      .post("/api/products")
      .send(sampleProduct);
    let _id = newProduct.body.data._id;
    const result = await request(app)
      .put("/api/products/" + _id)
      .send({ price: 1000 });
    expect(result.statusCode).toBe(200);
    expect(result.body.data).toHaveProperty("_id", _id);
    expect(result.body.data).toHaveProperty("productId", "88");
    expect(result.body.data).toHaveProperty("price", 1000);
  });
});

describe("DELETE /api/products/:id", () => {
  it("should return a 400 invalid id", async () => {
    const result = await request(app).delete("/api/products/invalid_id");
    expect(result.statusCode).toBe(400);
    expect(result.body.error.length).toBeGreaterThan(0);
  });

  it("should return a 404 product not found", async () => {
    const result = await request(app).delete(
      "/api/products/65250ad5c3e05d0ddb5bbfa0"
    );
    expect(result.statusCode).toBe(404);
  });

  it("should delete a product", async () => {
    let newProduct = await request(app)
      .post("/api/products")
      .send(sampleProduct);
    let _id = newProduct.body.data._id;
    const deletetResult = await request(app).delete("/api/products/" + _id);
    expect(deletetResult.statusCode).toBe(204);
    const getResult = await request(app).get("/api/products/" + _id);
    expect(getResult.statusCode).toBe(404);
  });
});
