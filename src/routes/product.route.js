import express from "express";
var productRouter = express.Router();

import {
  createProductHandler,
  getAllProductHandler,
  getIdProductHandler,
  updateProductHandler,
  deleteProductHandler,
} from "../controllers/product.controller.js";
import {
  createProductSchema,
  getAllProductSchema,
  getIdProductSchema,
  updateProductSchema,
  deleteProductSchema,
} from "../schemas/product.schema.js";
import validate from "../middlewares/validate.js";

/**
 * @openapi
 * '/api/products/{id}':
 *  get:
 *     tags:
 *     - Products
 *     summary: Get a single product by the _id
 *     parameters:
 *      - name: id
 *        in: path
 *        description: get a product by uuid _id
 *        required: true
 *     responses:
 *       200:
 *         description: Success
 *         content:
 *          application/json:
 *           schema:
 *              $ref: '#/components/schema/getIdProductResponse'
 *       404:
 *         description: Product not found
 *  put:
 *     tags:
 *     - Products
 *     summary: Update a single product
 *     parameters:
 *      - name: id
 *        in: path
 *        description: The _id of the product
 *        required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schema/updateProductRequest'
 *     responses:
 *       200:
 *         description: Success
 *         content:
 *          application/json:
 *           schema:
 *              $ref: '#/components/schema/getIdProductResponse'
 *       404:
 *         description: Product not found
 *  delete:
 *     tags:
 *     - Products
 *     summary: Delete a single product
 *     parameters:
 *      - name: id
 *        in: path
 *        description: The _id of the product
 *        required: true
 *     responses:
 *       200:
 *         description: Product deleted
 *       404:
 *         description: Product not found
 */

productRouter.get("/:id", validate(getIdProductSchema), getIdProductHandler);

productRouter.put("/:id", validate(updateProductSchema), updateProductHandler);

productRouter.delete(
  "/:id",
  validate(deleteProductSchema),
  deleteProductHandler
);

/**
 * @openapi
 * '/api/products/':
 *  get:
 *     tags:
 *     - Products
 *     summary: Get all products
 *     responses:
 *       200:
 *         description: Success
 *         content:
 *          application/json:
 *           schema:
 *              $ref: '#/components/schema/getAllProductResponse'
 *       404:
 *         description: Product not found
 *  post:
 *     tags:
 *     - Products
 *     summary: create a new product
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schema/updateProductRequest'
 *     responses:
 *       201:
 *         description: Success
 *         content:
 *          application/json:
 *           schema:
 *              $ref: '#/components/schema/getIdProductResponse'
 */

productRouter.get("/", validate(getAllProductSchema), getAllProductHandler);

productRouter.post("/", validate(createProductSchema), createProductHandler);

export default productRouter;
