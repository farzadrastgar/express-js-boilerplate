import connect from "./utils/db.js";
import logger from "./utils/logger.js";
import "dotenv/config";
import app from "./app.js";
const port = process.env.PORT;

app.listen(port, () => {
  logger.info(`Example app listening on port ${port}`);

  connect();
});
