import mongoose from "mongoose";

const productSchema = new mongoose.Schema({
  productId: {
    type: String,
    require: true,
    unique: true,
  },
  productName: {
    type: String,
    require: true,
    maxlength: 100,
  },
  description: {
    type: String,
    maxlength: 500,
  },
  price: {
    type: Number,
    required: true,
    min: 0,
  },
  category: {
    type: String,
    require: true,
    maxlength: 30,
  },
  stockQuantity: Number,
  manufacturer: String,
  ratings: {
    averageRating: Number,
    numRatings: Number,
  },
});

export default mongoose.model("Product", productSchema);
