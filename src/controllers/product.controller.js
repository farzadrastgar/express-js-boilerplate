import ProductModel from "../models/product.model.js";

export const createProductHandler = async (req, res, next) => {
  try {
    const product = new ProductModel(req.body);
    const savedProduct = await product.save();
    res.status(201).json({ success: true, data: savedProduct });
  } catch (err) {
    next(err);
  }
};

export const getAllProductHandler = async (req, res, next) => {
  try {
    let results = await ProductModel.find();
    res.status(200).json({ success: true, data: results || [] });
  } catch (err) {
    next(err);
  }
};

export const getIdProductHandler = async (req, res, next) => {
  try {
    let product = await ProductModel.findOne({ _id: req.params.id });
    if (product) {
      res.status(200).json({ success: true, data: product });
    } else {
      res.status(404).json({ success: false, message: "Product not found" });
    }
  } catch (err) {
    next(err);
  }
};

export const updateProductHandler = async (req, res, next) => {
  try {
    let product = await ProductModel.findOne({ _id: req.params.id });
    if (product) {
      for (const key in req.body) {
        product[key] = req.body[key];
      }
      const updatedProduct = await product.save();
      res.status(200).json({ success: true, data: updatedProduct });
    } else {
      res.status(404).json({ success: false, message: "Product not found" });
    }
  } catch (err) {
    next(err);
  }
};

export const deleteProductHandler = async (req, res, next) => {
  try {
    let result = await ProductModel.findOneAndDelete({ _id: req.params.id });
    if (result) {
      res.status(204).json({ success: true });
    } else {
      res.status(404).json({ success: false, message: "Product not found" });
    }
  } catch (err) {
    next(err);
  }
};
