import swaggerUi from "swagger-ui-express";
import swaggerJsdoc from "swagger-jsdoc";
import pkg from "../../package.json" assert { type: "json" };

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Express-mongodb REST API",
      version: pkg.version,
    },
  },
  apis: ["./src/routes/*.js", "./src/schemas/*.js"], // files containing annotations as above
};

const specs = swaggerJsdoc(options);

function swagger(app) {
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs));
}

export default swagger;
