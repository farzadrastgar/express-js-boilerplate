import chalk from "chalk";

// Define log levels and their corresponding colors
const logLevels = {
  info: chalk.blue,
  success: chalk.green,
  warn: chalk.yellow,
  error: chalk.red,
};

// Custom logger class with timestamp
class Logger {
  constructor() {
    this.timestamp = () => new Date().toLocaleString();
  }

  log(level, message) {
    const logFunction = logLevels[level] || chalk.white; // Default to white for unknown levels
    console.log(
      logFunction(`${this.timestamp()} [${level.toUpperCase()}]: ${message}`)
    );
  }

  info(message) {
    this.log("info", message);
  }

  success(message) {
    this.log("success", message);
  }

  warn(message) {
    this.log("warn", message);
  }

  error(message) {
    this.log("error", message);
  }
}

export default new Logger();
