import mongoose from "mongoose";
import logger from "./logger.js";
const db = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI);
    logger.info("connected to db");
  } catch (err) {
    logger.error("failed to connect to db");
    process.exit(1);
  }
};

export default db;
